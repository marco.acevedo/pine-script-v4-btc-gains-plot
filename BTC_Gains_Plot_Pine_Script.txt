// This source code is subject to the terms of the Mozilla Public License 2.0 at https://mozilla.org/MPL/2.0/
// © frameinspanish

//@version=4
study("BTC Gain",overlay=false)

////////////////////////////////////////////////////////////////////////////////
//This was posted for the entertainment purposes of IntoTheCryptoVerse subreddit
//Follow me or contact me, I am @frameinspanish almost everywhere
////////////////////////////////////////////////////////////////////////////////
//This is irrelevant data for this indicator, but I use it for personal purposes
myBreakEvenPrice=38987.94
shares = 0.057//coins purchased at Coinbase
cost   = 2207.9//my current cost at coinbase
////////////////////////////////////////////////////////////////////////////////

//These are my default values for the total numer of coins I own
numCoinsIn  = input(title="Number of Coins",type = input.float,defval=0.05847847)

//This is the total USD amount I paid for the number of coins above
totalCostIn = input(title="Total Cost",type = input.float,defval=2207.9)

//This is how much my coins are worth at the current price
myCashValue = close*numCoinsIn

//This is my technical break even price not including exchange and gas fees
bep = totalCostIn/numCoinsIn

//Declaration of color variables to later change depending on positive or negative gain
var color c  = na
var color c2 = color.yellow

//Calculates the Gain and also shows high and low candles as upper and lower bounds
GainH=(high/bep)//-1)//*100
GainC=(close/bep)//-1)//*100
GainL=(low/bep)//-1)//*100

//Logic for coloring the plot based on closing price
if (GainC>1)
    c := color.green
else
    c := color.red


//plots the gain over time as well as the break even line
plot(1,"Break Even",color.gray,linewidth=2)
PH = plot(GainH,"Gain",color.gray,linewidth=2)
PC = plot(GainC,"Gain",c,linewidth=2)
PL = plot(GainL,"Gain",color.gray,linewidth=2)

//Logic for coloring the shadow between 2 plots
//If during the indicator time frame, the gain was positive and negative the color is yellow
//If the gain was positive the shadow is green
//If the gain was negative, the color of the shadow is red
if(GainL>1 and GainH>1)
    c2 := color.green
if(GainH<1 and GainL<1)
    c2 := color.red
if(GainH>1 and GainL<1)
    c2 := color.yellow

//fills the color between the high and low candle prices according to the logic above
fill(PH,PL,c2)